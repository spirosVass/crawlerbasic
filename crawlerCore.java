import java.util.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class crawlerCore {
    //Create arraylist to save related links within the domain in order to call parser recursively.	
	public static ArrayList<String> arrayL = new ArrayList<String>();
	public static String URL_import;
	
	public void parser(String URL) throws IOException{
		
		//final String 
		//Using Jsoup in order to get the html page and further analyze it.
		Document html_doc = Jsoup.connect(URL).get();
		
		//Getting the links from the page saving them in links variable
		Elements links = html_doc.select("a[href]");
		
		//Getting the static content of html page.
        Elements static_content = html_doc.select("[src]");

        //Printing out static content separating images from other types (etc scripts)
        System.out.println("Static Content Size: "+static_content.size());
        for (Element content : static_content) {
            if (content.tagName().equals("img"))
            	System.out.println("Image content: "+content.attr("abs:src"));
            else
            	System.out.println("Other static Content: "+content.tagName()+" "+content.attr("abs:src"));
        }
        //Print out the links from html page except for the ones that will be visited.
        System.out.println("\nLinks:");
        for (Element link : links){
         	if (!link.attr("abs:href").matches(URL_import+".*")){
               	System.out.println("Other Link: "+link.tagName()+" "+link.attr("abs:href"));
            }	
       	}
        //Calling parser recursively to visit web pages within the domain that have not been visited before 
        for (Element link : links){
        	if (link.attr("abs:href").matches(URL_import+".*") && !arrayL.contains(link.attr("abs:href"))){
        		System.out.println("\nBasic Link: "+link.attr("abs:href"));
        		arrayL.add(link.attr("abs:href"));
        		parser(link.attr("abs:href"));
        	}
		}
	}
}

class TestCrawler{
	
	public static void main(String args[]){
		//Getting initial web page from the command line.
		crawlerCore.URL_import=args[0];
				
		try{
			//Calling parser using http://wiprodigital.com as starting page.
			System.out.println("Basic URL: "+crawlerCore.URL_import);
			crawlerCore spider = new crawlerCore();
			spider.parser(crawlerCore.URL_import);
		}
		catch(IOException e){
		}
	}
}
