# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
This repository contains a simple crawler that may be used in order to provide all the links contained in a single domain. Moreover prints out
other content like images, scripts etc.
It uses Java Jsoup library.

### How do I get set up? ###
Once crawlerCore.java gets compiled it needs to be run with just one argument which is the basic URL we want to dig in to. 
Please provide the argument without "/" at the end. 

### Contribution guidelines ###
At the moment there are not test cases provided.